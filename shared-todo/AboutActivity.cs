﻿using AbdulAris.Civ;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.Core.View;
using shared_todo.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo
{
    [Activity(Label = "AboutActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AboutActivity : Activity
    {
        private TextView title, version, creator;
        private ImageView goBack;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_about);
            InitStatusBar();

            title = FindViewById<TextView>(Resource.Id.tvTitle);
            version = FindViewById<TextView>(Resource.Id.tvVersion);
            creator = FindViewById<TextView>(Resource.Id.tvMadeBy);
            goBack = FindViewById<ImageView>(Resource.Id.ibGoBack);            

            goBack.Click += GoBackToPreviousScreen;

            InitFonts();
        }

        /// <summary>
        /// Initializes fonts required for all text on screen 
        /// </summary>
        private void InitFonts()
        {
            Typeface titleTypeface = Typeface.CreateFromAsset(this.Assets, "Nexa Bold.otf");
            Typeface textTypeface = Typeface.CreateFromAsset(this.Assets, "Montserrat-Regular.ttf");

            title.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            version.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            creator.SetTypeface(textTypeface, TypefaceStyle.Normal);
        }

        /// <summary>
        /// Return to settings screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoBackToPreviousScreen(object sender, EventArgs e)
        {
            Finish();
        }

        /// <summary>
        /// Initialize transparent status bar
        /// </summary>
        public void InitStatusBar()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
                var s = SystemUiFlags.LayoutFullscreen | SystemUiFlags.LayoutStable;
                Window.SetDecorFitsSystemWindows(false);
                FindViewById(Android.Resource.Id.Content).SystemUiVisibility = (StatusBarVisibility)s;
            }
        }
    }
}