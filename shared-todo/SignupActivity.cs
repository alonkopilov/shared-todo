﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using shared_todo.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace shared_todo
{
    [Activity(Label = "SignupActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SignupActivity : Activity
    {
        private FirebaseHelper firebaseHelper;
        private ProgressBar progressBar;
        private EditText username, password, email;
        private Button goBack, signup;
        private TextView title;
        private Random randInt = new Random();
        private ISharedPreferences sp;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_signup);
            InitStatusBar();

            username = FindViewById<EditText>(Resource.Id.etSignupUsername);
            password = FindViewById<EditText>(Resource.Id.etSignupPassword);
            email = FindViewById<EditText>(Resource.Id.etSignupEmail);
            signup = FindViewById<Button>(Resource.Id.performSignUp);
            goBack = FindViewById<Button>(Resource.Id.back);
            title = FindViewById<TextView>(Resource.Id.tvTitle);
            progressBar = FindViewById<ProgressBar>(Resource.Id.pBar);
            sp = this.GetSharedPreferences("details", FileCreationMode.Private);
            firebaseHelper = new FirebaseHelper(this);

            progressBar.Visibility = ViewStates.Invisible;

            signup.Click += PerformSignup;
            goBack.Click += GoToLogin;

            InitFonts();
        }

        /// <summary>
        /// Initializes fonts required for all text on screen 
        /// </summary>
        private void InitFonts()
        {
            Typeface titleTypeface = Typeface.CreateFromAsset(this.Assets, "Nexa Bold.otf");
            Typeface textTypeface = Typeface.CreateFromAsset(this.Assets, "Montserrat-Regular.ttf");
            title.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            signup.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            goBack.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            username.SetTypeface(textTypeface, TypefaceStyle.Normal);
            password.SetTypeface(textTypeface, TypefaceStyle.Normal);
            email.SetTypeface(textTypeface, TypefaceStyle.Normal);
        }

        /// <summary>
        /// Directs to the login screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoToLogin(object sender, EventArgs e)
        {
            Intent iLogin = new Intent(this, typeof(MainActivity));

            StartActivity(iLogin);
        }

        /// <summary>
        /// Performs a signup
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private async void PerformSignup(object sender, EventArgs e)
        {
            progressBar.Visibility = ViewStates.Visible;
            bool userExists = await firebaseHelper.CheckIfUserExist(username.Text);

            if (userExists)
            {
                Toast.MakeText(this, "Username already taken.", ToastLength.Long).Show();
                username.Text = string.Empty;
                return;
            }
            if (CheckValidDetails())
            {
                DatabaseReference reference = firebaseHelper.GetDatabase().GetReference("Users").Push();
                User userToAdd = new User(username.Text, password.Text, email.Text, randInt.Next(1, 10000).ToString("D4"));

                firebaseHelper.AddUser(reference, ref userToAdd);
                Toast.MakeText(this, "You have signed up successfully!", ToastLength.Long).Show();

                Intent iYourTasks = new Intent(this, typeof(YourTasksActivity));
                iYourTasks.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);
                MainActivity.loginUser = userToAdd;

                RememberUser(username.Text);
                StartActivity(iYourTasks);
                
                username.Text = string.Empty;
                password.Text = string.Empty;
                email.Text = string.Empty;
            }
            progressBar.Visibility = ViewStates.Invisible;
        }

        /// <summary>
        /// Remembers the user in a SharedPreferences for automatic logins.
        /// </summary>
        /// <param name="username">string username to remember</param>
        public void RememberUser(string username)
        {
            ISharedPreferencesEditor editor = sp.Edit();

            editor.PutBoolean("isRemembered", true);
            editor.PutString("currentLoginedUser", username);
            editor.Commit();
        }

        /// <summary>
        /// Checks if all the information given in signup is valid
        /// </summary>
        /// <returns>true if all the details are valid, else false</returns>
        private bool CheckValidDetails()
        {
            Regex rgValidEmail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (username.Text == string.Empty || password.Text == string.Empty || email.Text == string.Empty)
            {
                Toast.MakeText(this, "Please fill all fields.", ToastLength.Long).Show();
                return false;
            }
            if (username.Text.Length < 3 || password.Text.Length < 3)
            {
                Toast.MakeText(this, "Username and password must be above 3 characters.", ToastLength.Long).Show();
                return false;
            }
            if (!rgValidEmail.IsMatch(email.Text))
            {
                Toast.MakeText(this, "Invalid email address", ToastLength.Long).Show();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Initialize transparent status bar
        /// </summary>
        public void InitStatusBar()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
                var s = SystemUiFlags.LayoutFullscreen | SystemUiFlags.LayoutStable;
                FindViewById(Android.Resource.Id.Content).SystemUiVisibility = (StatusBarVisibility)s;
            }
        }
    }
}