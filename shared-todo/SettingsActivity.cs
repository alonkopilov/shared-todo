﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo
{
    [Activity(Label = "SettingsActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SettingsActivity : Activity
    {
        private TableRow account, joinList, about, logout;
        private ImageView goBack;
        private TextView title;
        private ISharedPreferences sp;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_settings);
            InitStatusBar();

            title = FindViewById<TextView>(Resource.Id.tvSettingsTitle);
            account = FindViewById<TableRow>(Resource.Id.trAccount);
            joinList = FindViewById<TableRow>(Resource.Id.trJoinList);
            about = FindViewById<TableRow>(Resource.Id.trAbout);
            logout = FindViewById<TableRow>(Resource.Id.trLogout);
            goBack = FindViewById<ImageView>(Resource.Id.ibGoBack);
            sp = this.GetSharedPreferences("details", FileCreationMode.Private);

            account.Click += GoToYourAccount;
            joinList.Click += GoToJoinList;
            goBack.Click += GoBackToPreviousScreen;
            about.Click += GoToAbout;
            logout.Click += SignoutFromApp;

            InitFonts();
        }

        /// <summary>
        /// Directs to the "About" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoToAbout(object sender, EventArgs e)
        {
            Intent iAbout = new Intent(this, typeof(AboutActivity));

            StartActivity(iAbout);
        }

        /// <summary>
        /// Signs the user out of the app
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void SignoutFromApp(object sender, EventArgs e)
        {
            Intent iSignout = new Intent(this, typeof(MainActivity));
            ISharedPreferencesEditor editor = sp.Edit();

            iSignout.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask); //Empty activity stack
            MainActivity.loginUser = null;

            YourTasksActivity.exit = true; //Stop task updater thread
            editor.PutBoolean("isRemembered", false);
            editor.PutString("currentLoginedUser", "");
            editor.Commit();

            StartActivity(iSignout);
        }

        /// <summary>
        /// Directs to the "Your Tasks" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoBackToPreviousScreen(object sender, EventArgs e)
        {
            Intent iYourTasks = new Intent(this, typeof(YourTasksActivity));
            iYourTasks.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);

            StartActivity(iYourTasks);
        }

        /// <summary>
        /// Initializes fonts required for all text on screen 
        /// </summary>
        private void InitFonts()
        {
            Typeface titleTypeface = Typeface.CreateFromAsset(this.Assets, "Montserrat-Regular.ttf");
            title.SetTypeface(titleTypeface, TypefaceStyle.Normal);

            ((TextView)about.GetChildAt(1)).SetTypeface(titleTypeface, TypefaceStyle.Normal);
            ((TextView)joinList.GetChildAt(1)).SetTypeface(titleTypeface, TypefaceStyle.Normal);
            ((TextView)account.GetChildAt(1)).SetTypeface(titleTypeface, TypefaceStyle.Normal);
            ((TextView)logout.GetChildAt(1)).SetTypeface(titleTypeface, TypefaceStyle.Normal);
        }

        /// <summary>
        /// Directs to the "Join List" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoToJoinList(object sender, EventArgs e)
        {
            Intent iJoinList = new Intent(this, typeof(JoinListActivity));

            StartActivity(iJoinList);
        }

        /// <summary>
        /// Directs to the "Account" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoToYourAccount(object sender, EventArgs e)
        {
            Intent iYourAccount = new Intent(this, typeof(YourAccountActivity));

            StartActivity(iYourAccount);
        }

        /// <summary>
        /// Initialize transparent status bar
        /// </summary>
        public void InitStatusBar()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
                var s = SystemUiFlags.LayoutFullscreen | SystemUiFlags.LayoutStable;
                FindViewById(Android.Resource.Id.Content).SystemUiVisibility = (StatusBarVisibility)s;
            }
        }

    }
}