﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using AndroidX.Core.App;
using AndroidX.RecyclerView.Widget;
using shared_todo.Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Thread = System.Threading.Thread;

namespace shared_todo
{
    [Activity(Label = "YourTasksActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class YourTasksActivity : Activity
    {
        public static List<Task> tasksList { get; set; }
        public static bool exit;
        public static User loginUser;
        private FirebaseHelper firebaseHelper;
        private TasksAdapter tasksAdapter;
        private TextView welcomeMsg, yourTasksTitle, battery, noTasksMessage;
        private ImageButton settings, addTask;
        private ImageView isCharging;  
        private BroadcastBattery broadcastB;
        private readonly string[] permissions = { Manifest.Permission.ReadPhoneState };

        private RecyclerView rv;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_yourtasks);
            InitStatusBar();

            ActivityCompat.RequestPermissions(this, permissions, 1);
            settings = FindViewById<ImageButton>(Resource.Id.ivSettings);
            addTask = FindViewById<ImageButton>(Resource.Id.ibtnAddTask);
            welcomeMsg = FindViewById<TextView>(Resource.Id.tvTitle);
            addTask = FindViewById<ImageButton>(Resource.Id.ibtnAddTask);
            yourTasksTitle = FindViewById<TextView>(Resource.Id.tvTasksTitle);
            battery = FindViewById<TextView>(Resource.Id.tvBatteryPercent);
            isCharging = FindViewById<ImageView>(Resource.Id.tvIsCharging);
            noTasksMessage = FindViewById<TextView>(Resource.Id.tvNoTasksMsg);
            rv = FindViewById<RecyclerView>(Resource.Id.rvTasks);
            firebaseHelper = new FirebaseHelper(this);
            broadcastB = new BroadcastBattery(battery, isCharging);
            tasksList = new List<Task>();

            settings.Click += GoToSettings;
            addTask.Click += GoToAddTask;

            exit = false;
            rv.SetLayoutManager(new GridLayoutManager(this, 1));

            changeTaskListVisibility(tasksList);
            InitTitle();
            InitAdapter();
            InitTaskUpdater();
            InitFonts();
        }

        /// <summary>
        /// Initializes the main screen welcome title
        /// </summary>
        public void InitTitle()
        {
            welcomeMsg.Text = "Welcome, " + char.ToUpper(MainActivity.loginUser.username[0]) + MainActivity.loginUser.username[1..];
        }

        /// <summary>
        /// Initializes the tasks updater thread.
        /// </summary>
        public void InitTaskUpdater()
        {
            Thread thr = new Thread(new ThreadStart(UpdateTaskList));
            thr.SetApartmentState(ApartmentState.STA);
            thr.Start();
        }

        /// <summary>
        /// Initializes the tasks ListView adapter
        /// </summary>
        private void InitAdapter()
        {
            tasksAdapter = new TasksAdapter(this, tasksList);
            rv.SetAdapter(tasksAdapter);

            SwipeController swipeController = new SwipeController(tasksAdapter, this);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeController);
            itemTouchHelper.AttachToRecyclerView(rv);
        }

        /// <summary>
        /// Initialize transparent status bar
        /// </summary>
        public void InitStatusBar()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
                var s = SystemUiFlags.LayoutFullscreen | SystemUiFlags.LayoutStable;
                FindViewById(Android.Resource.Id.Content).SystemUiVisibility = (StatusBarVisibility)s;
            }
        }

        /// <summary>
        /// Initializes fonts required for all text on screen 
        /// </summary>
        private void InitFonts()
        {
            Typeface textTypeface = Typeface.CreateFromAsset(this.Assets, "Montserrat-Regular.ttf");

            welcomeMsg.SetTypeface(textTypeface, TypefaceStyle.Normal);
            yourTasksTitle.SetTypeface(textTypeface, TypefaceStyle.Normal);
            battery.SetTypeface(textTypeface, TypefaceStyle.Bold);
        }

        /// <summary>
        /// Tasks list updater thread. This thread works asynchronously, it sends every three seconds 
        /// a request to the database to get the updated list's tasks, and updates the UI accordingly.
        /// This thread finishes when the app gets closed, or when the user signed out.
        /// </summary>
        private void UpdateTaskList()
        {
            while (!exit)
            {
                RunOnUiThread(async () =>
                {
                    //Get updated task list
                    List<Task> tasks = await firebaseHelper.GetTaskList(MainActivity.loginUser.listCode);

                    tasksList.Clear();
                    if (tasks != null)
                    {
                        tasksList.AddRange(tasks);
                    }              

                    //Update UI
                    changeTaskListVisibility(tasks);
                    tasksAdapter.NotifyDataSetChanged();
                });
                Thread.Sleep(3000);
            }

        }

        /// <summary>
        /// Checks if the user has any tasks.
        /// If the user has tasks, the app shows them on the screen, else, an appropriate message is shown.
        /// </summary>
        /// <param name="tasks">The list of tasks</param>
        public void changeTaskListVisibility(List<Task> tasks)
        {
            if (tasks == null || !tasks.Any())
            {
                noTasksMessage.Visibility = ViewStates.Visible;
                rv.Visibility = ViewStates.Gone;
            }
            else
            {
                noTasksMessage.Visibility = ViewStates.Gone;
                rv.Visibility = ViewStates.Visible;
            }
        }

        /// <summary>
        /// Directs to the "Add Task" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoToAddTask(object sender, EventArgs e)
        {
            Intent iAddTask = new Intent(this, typeof(AddTaskActivity));

            StartActivity(iAddTask);
        }

        /// <summary>
        /// Directs to the "Settings" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoToSettings(object sender, EventArgs e)
        {
            Intent iSettings = new Intent(this, typeof(SettingsActivity));

            StartActivity(iSettings);
        }

        /// <summary>
        /// Sets app to update battery and list of tasks on resume
        /// </summary>
        protected override void OnResume()
        {
            base.OnResume();
            if (tasksAdapter != null)
            {
                tasksAdapter.NotifyDataSetChanged();
            }
            RegisterReceiver(broadcastB, new IntentFilter(Intent.ActionBatteryChanged));
        }

        /// <summary>
        /// Sets app to stop updating battery on pause
        /// </summary>
        protected override void OnPause()
        {
            UnregisterReceiver(broadcastB);
            base.OnPause();
        }

        /// <summary>
        /// Initializes permissions after request.
        /// </summary>
        /// <param name="requestCode">int request code</param>
        /// <param name="permissions">string array of permissions requested</param>
        /// <param name="grantResults">Permissions array of all denied and granted permissions</param>
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}