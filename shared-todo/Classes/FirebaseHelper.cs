﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Database;
using Java.IO;
using Java.Util;


namespace shared_todo.Classes
{
    class FirebaseHelper
    {
        private readonly Context appContext;
        private UsersListener usersListener;
        private TaskListsListener taskListsListener;
        private List<User> users;
        private List<TaskList> taskLists;

        public FirebaseHelper(Context appContext)
        {
            this.appContext = appContext;
        }
        
        /// <summary>
        /// Retrieves data from the database to the class lists
        /// </summary>
        public void RetrieveData()
        {
            usersListener = new UsersListener();
            taskListsListener = new TaskListsListener(appContext);

            usersListener.Create();
            taskListsListener.Create();

            usersListener.UsersRetrived += GetUsersDataFromInvoke;
            taskListsListener.ListsRetrived += GetListsDataFromInvoke;
        }

        /// <summary>
        /// Sets the list of Task lists to the data from the database after the event was triggered
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event details and args</param>
        private void GetListsDataFromInvoke(object sender, TaskListsListener.ListsDataEventArgs e)
        {
            taskLists = e.allTasksLists;
        }

        /// <summary>
        /// Sets the list of users to the data from the database after the event was triggered
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event details and args</param>
        private void GetUsersDataFromInvoke(object sender, UsersListener.UsersDataEventArgs e)
        {
            users = e.allUsers;
        }
        
        /// <summary>
        /// Adds a given user to the database
        /// </summary>
        /// <param name="reference">Reference to the users collection in the database</param>
        /// <param name="userToAdd">User object to add</param>
        public void AddUser(DatabaseReference reference, ref User userToAdd)
        {
            HashMap newUser = new HashMap();
            string listCode = CreateRandomListCode();

            newUser.Put("USERNAME", userToAdd.username);
            newUser.Put("PASSWORD", userToAdd.password);
            newUser.Put("EMAIL", userToAdd.email);
            newUser.Put("LISTCODE", listCode);
            userToAdd.listCode = listCode;

            AddList(GetDatabase().GetReference("Lists").Push(), listCode);

            reference.SetValue(newUser);
        }
        
        /// <summary>
        /// Updates a given profile image to a given user
        /// </summary>
        /// <param name="user">User to change it's image</param>
        /// <param name="img">Bitmap image to change to</param>
        public void UpdatePicToUser(User user, Bitmap img)
        {
            IDictionary<string, Java.Lang.Object> keyValuePairs = new Dictionary<string, Java.Lang.Object>
            {
                { "PROFILE_IMG", BitMapToString(img) }
            };

            GetDatabase().GetReference("Users").Child(user.userId).UpdateChildren(keyValuePairs);
        }

        /// <summary>
        /// Returns a given user's profile image from the database
        /// </summary>
        /// <param name="userToGet">User to get its profile image</param>
        /// <returns>Bitmap profile image of the user</returns>
        public async Task<Bitmap> GetUserPic(User userToGet)
        {
            RetrieveData();

            while (users == null)
            {
                await System.Threading.Tasks.Task.Delay(25);
            }
            foreach (User user in users)
            {
                if (user.userId == userToGet.userId)
                {
                    return user.profilePic;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the list of users that are in a given list
        /// </summary>
        /// <param name="listCode">The code of the list to check</param>
        /// <returns>List of users in that list</returns>
        public async Task<List<User>> GetUsersInList(string listCode)
        {
            RetrieveData();

            while (users == null)
            {
                await System.Threading.Tasks.Task.Delay(25);
            }
            List<User> usersInTaskList = new List<User>();
            foreach (User user in users)
            {
                if (user.listCode == listCode)
                {
                    usersInTaskList.Add(user);
                }
            }
            return usersInTaskList;
        }

        /// <summary>
        /// Decodes an encoded Base64 image string to a Bitmap image
        /// </summary>
        /// <param name="encodedString">Encoded Base64 image string</param>
        /// <returns>Decoded bitmap image</returns>
        public Bitmap StringToBitMap(string encodedString)
        {
            byte[] imageAsBytes = Java.Util.Base64.GetDecoder().Decode(encodedString);

            return BitmapFactory.DecodeByteArray(imageAsBytes, 0, imageAsBytes.Length);
        }

        /// <summary>
        /// Encodes a Bitmap image to a Base64 encoded string
        /// </summary>
        /// <param name="bitmap">Bitmap image to encode</param>
        /// <returns>Encoded string in base64</returns>
        public string BitMapToString(Bitmap bitmap)
        {
            MemoryStream byteArrayOutputStream = new MemoryStream();
            bitmap.Compress(Bitmap.CompressFormat.Png, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.ToArray();

            return System.Text.Encoding.Default.GetString(Java.Util.Base64.GetEncoder().Encode(byteArray));
        }

        /// <summary>
        /// Adds a new task list to the database with a given list code
        /// </summary>
        /// <param name="reference">Reference to the task lists collection in the database</param>
        /// <param name="listCode">The list code of the list to add</param>
        public void AddList(DatabaseReference reference, string listCode)
        {
            HashMap newTaskList = new HashMap();

            newTaskList.Put("LISTCODE", listCode);
            newTaskList.Put("TASKS", new HashMap());

            reference.SetValue(newTaskList);
        }
        
        /// <summary>
        /// Adds a task to a list in the database
        /// </summary>
        /// <param name="reference">Reference to the tasks property of a given list</param>
        /// <param name="taskToAdd">Task object to add to the database</param>
        /// <param name="listKey">The list key of the list to add the task to</param>
        public void AddTaskToList(DatabaseReference reference, Task taskToAdd, string listKey)
        {
            if (reference is null)
            {
                throw new ArgumentNullException(nameof(reference));
            }

            reference = GetDatabase().GetReference("Lists").Child(listKey).Child("TASKS").Push();
            HashMap newTask = new HashMap();

            newTask.Put("NAME", taskToAdd.Name);
            newTask.Put("CATEGORY", taskToAdd.Category);
            newTask.Put("DESCRIPTION", taskToAdd.Description);
            newTask.Put("COLOR", taskToAdd.Color);
            newTask.Put("DUE_DATE", (taskToAdd.DueDate == null) ? new Date(0,0,0).ToString() : taskToAdd.DueDate.ToString());
            newTask.Put("IS_COMPLETED", taskToAdd.IsCompleted);
            
            reference.SetValue(newTask);
        }

        /// <summary>
        /// Updates an existing task in the database
        /// </summary>
        /// <param name="taskToUpdate">Task object to update</param>
        /// <param name="listKey">The key of the list the task belongs to</param>
        public void UpdateExistingTask(Task taskToUpdate, string listKey)
        {
            IDictionary<string, Java.Lang.Object> keyValuePairs = new Dictionary<string, Java.Lang.Object>
            {
                { "NAME", taskToUpdate.Name },
                { "CATEGORY", taskToUpdate.Category },
                { "DESCRIPTION", taskToUpdate.Description },
                { "COLOR", taskToUpdate.Color },
                { "DUE_DATE", (taskToUpdate.DueDate == null) ? new Date(0, 0, 0).ToString() : taskToUpdate.DueDate.ToString() },
                { "IS_COMPLETED", taskToUpdate.IsCompleted }
            };

            GetDatabase().GetReference("Lists").Child(listKey).Child("TASKS").Child(taskToUpdate.TaskId).UpdateChildren(keyValuePairs);
        }

        public void SetTaskStatus(bool isCompleted)
        {

        }

        /// <summary>
        /// Removes a task from a list in the database
        /// </summary>
        /// <param name="listCode">Code of the list to remove the task from</param>
        /// <param name="taskId">The id of the task to remove</param>
        public void RemoveTask(string listCode, string taskId)
        {
            try
            { 
                GetDatabase().GetReference("Lists").Child(GetTaskListKey(listCode).Result).Child("TASKS").Child(taskId).RemoveValue();

            }
            catch
            {
                Toast.MakeText(this.appContext, "Error deleting task, try again.", ToastLength.Short);
            }
        }

        /// <summary>
        /// Returns a list of tasks from a given list code
        /// </summary>
        /// <param name="listCode">code string of the list to get</param>
        /// <returns>The list of tasks</returns>
        public async Task<List<Task>> GetTaskList(string listCode)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            RetrieveData();

            while (taskLists == null)
            {
                await System.Threading.Tasks.Task.Delay(25);
            }
            foreach (TaskList taskList in taskLists)
            {
                if (taskList.ListCode == listCode)
                {
                    return taskList.Tasks;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns the list key of a list given its code
        /// </summary>
        /// <param name="listCode">code of the list to get its key from</param>
        /// <returns>key of taskslist</returns>
        public async Task<string> GetTaskListKey(string listCode)
        {
            RetrieveData();

            while (taskLists == null)
            {
                await System.Threading.Tasks.Task.Delay(25);
            }
            foreach (TaskList taskList in taskLists)
            {
                if (taskList.ListCode == listCode)
                {
                    return taskList.TasksListId;
                }
            }
            return "";
        }

        private static readonly System.Random rand = new System.Random();
        /// <summary>
        /// Creates a random list code string
        /// </summary>
        /// <returns>Random 6 digit code</returns>
        public string CreateRandomListCode()
        {
            const string allowedChars = "0123456789";
            int listCodeLen = 6;
            string listCode = "";

            for (int i = 0; i < listCodeLen; i++)
            {
                listCode += allowedChars[rand.Next(0, allowedChars.Length)];
            }

            return listCode;
        }

        /// <summary>
        /// Returns a user object of a given username from the database
        /// </summary>
        /// <param name="username">username of the user to get it's object from</param>
        /// <returns>The user object</returns>
        public async Task<User> GetUser(string username)
        {
            RetrieveData();

            while (users == null)
            {
                await System.Threading.Tasks.Task.Delay(25);
            }
            foreach (User user in users)
            {
                if (user.username == username)
                {
                    return user;
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if a given username exists in the database
        /// </summary>
        /// <param name="username">Username string to check</param>
        /// <returns>true if a user with the given username exists in the database, else false</returns>
        public async Task<bool> CheckIfUserExist(string username)
        {
            RetrieveData();

            while (users == null)
            {
                await System.Threading.Tasks.Task.Delay(25);
            }

            foreach (User user in users)
            {
                if (user.username == username)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Changes the list of a user
        /// </summary>
        /// <param name="user">A user object to change it's list</param>
        /// <param name="newListCode">The list code to join</param>
        /// <returns>true if the list exists, else false</returns>
        public async Task<bool> JoinListForUser(User user, string newListCode)
        {
            RetrieveData();

            while (taskLists == null && users == null)
            {
                await System.Threading.Tasks.Task.Delay(25);
            }

            foreach (TaskList taskList in taskLists) // Check if list with that code exists
            {
                if (taskList.ListCode == newListCode)
                {
                    UpdateListCodeForUser(user, newListCode);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Updates a given list code to a user
        /// </summary>
        /// <param name="user">User object to update it's list</param>
        /// <param name="newListCode">List code string</param>
        public void UpdateListCodeForUser(User user, string newListCode)
        {
            IDictionary<string, Java.Lang.Object> keyValuePairs = new Dictionary<string, Java.Lang.Object>
            {
                { "LISTCODE", newListCode }
            };

            GetDatabase().GetReference("Users").Child(user.userId).UpdateChildren(keyValuePairs);
        }

        /// <summary>
        /// Returns a database instance
        /// </summary>
        /// <returns>FirebaseDatabase object instance</returns>
        public FirebaseDatabase GetDatabase()
        {
            var app = FirebaseApp.InitializeApp(Application.Context);
            FirebaseDatabase database;

            if (app == null)
            {
                var dbProperties = new FirebaseOptions.Builder()
                    .SetApplicationId("com.companyname.shared_todo")
                    .SetApiKey("AIzaSyAHuRtyK2UNWqNzAYvmHMCTvfDlR-WV350")
                    .SetDatabaseUrl("https://db-shared-todo-default-rtdb.firebaseio.com/")
                    .SetStorageBucket("db-shared-todo.appspot.com")
                    .Build();
                app = FirebaseApp.InitializeApp(Application.Context, dbProperties);
            }

            database = FirebaseDatabase.GetInstance(app);


            return database;
        }

    }
} 
