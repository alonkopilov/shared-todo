﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AndroidX.RecyclerView.Widget;

namespace shared_todo.Classes
{
    class SwipeController : ItemTouchHelper.Callback
    {
        private TasksAdapter connectedAdapter;
        private FirebaseHelper firebaseHelper;

        public SwipeController(TasksAdapter adapter, Context context)
        {
            connectedAdapter = adapter;
            firebaseHelper = new FirebaseHelper(context);
        }
        /// <summary>
        /// Returns the supported movement flags
        /// </summary>
        /// <param name="p0">The RecyclerView object of the list</param>
        /// <param name="p1">ViewHolder to apply movement flags on</param>
        /// <returns>Correct Movement flags</returns>
        public override int GetMovementFlags(RecyclerView p0, RecyclerView.ViewHolder p1)
        {
            return MakeMovementFlags(0, ItemTouchHelper.Left | ItemTouchHelper.Right);
        }

        public override bool OnMove(RecyclerView p0, RecyclerView.ViewHolder p1, RecyclerView.ViewHolder p2)
        {
            return false;
        }

        /// <summary>
        /// Removes the ViewHolder on a RecyclerView item swipe
        /// </summary>
        /// <param name="p0">ViewHolder swiped</param>
        /// <param name="p1">swipe direction</param>
        public override async void OnSwiped(RecyclerView.ViewHolder p0, int p1)
        {
            Task swipedTask = YourTasksActivity.tasksList[p0.LayoutPosition];

            //Remove task from screen and database
            YourTasksActivity.tasksList.RemoveAt(p0.LayoutPosition);
            connectedAdapter.NotifyDataSetChanged();
            await System.Threading.Tasks.Task.Run(() => firebaseHelper.RemoveTask(MainActivity.loginUser.listCode, swipedTask.TaskId));
        }
    }
}