﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo.Classes
{
    class TasksAdapter : RecyclerView.Adapter
    {
        private List<Task> tasks;
        private Context context;
        private FirebaseHelper firebaseHelper;

        public TasksAdapter(Context context, List<Task> tasks)
        {
            this.tasks = tasks;
            this.context = context;
            firebaseHelper = new FirebaseHelper(context);
        }

        public List<Task> GetList()
        {
            return this.tasks;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        /// <summary>
        /// Binds a viewholder and a task object in the same position in the list
        /// </summary>
        /// <param name="holder">ViewHolder to apply the task details on</param>
        /// <param name="position">position of the ViewHolder on the list</param>
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            TaskViewHolder taskViewHolder = holder as TaskViewHolder;

            Task temp = this.tasks[position];
            if (temp != null)
            {                   // Update all fields in the view.
                Bitmap img = BitmapFactory.DecodeResource(Application.Context.Resources, context.Resources.GetIdentifier(temp.GetTickImgFilePath(), "drawable", context.PackageName));
                taskViewHolder.ivTickImg.SetImageBitmap(img);
                if (temp.DueDate != null)
                {
                    taskViewHolder.tvDueDate.Text = temp.DueDate.ToString();
                }
                taskViewHolder.tvTaskName.Text = temp.Name;
                taskViewHolder.tvTaskName.PaintFlags = temp.IsCompleted ? PaintFlags.StrikeThruText : PaintFlags.LinearText;
            }

            taskViewHolder.tvTaskName.Tag = holder.LayoutPosition;
            taskViewHolder.tvDueDate.Tag = holder.LayoutPosition;
            taskViewHolder.ivTickImg.Tag = holder.LayoutPosition;
            taskViewHolder.layout.Tag = holder.LayoutPosition;

            ApplyViewHolderClicks(taskViewHolder);
        }

        /// <summary>
        /// Applies the click and long click events to the view holder
        /// </summary>
        /// <param name="taskViewHolder">ViewHolder to apply click events to its properties</param>
        private void ApplyViewHolderClicks(TaskViewHolder taskViewHolder)
        {
            View[] holderViews = new View[] { taskViewHolder.tvTaskName, taskViewHolder.tvDueDate, taskViewHolder.ivTickImg, taskViewHolder.layout};

            // Fix and apply event holders
            foreach (View view in holderViews)
            {
                view.Click -= Task_ClickAsync;
                view.LongClick -= Task_LongClick;
                view.Click += Task_ClickAsync;
                view.LongClick += Task_LongClick;
            }
        }

        /// <summary>
        /// Opens the task editing screen on the long-clicked task
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void Task_LongClick(object sender, View.LongClickEventArgs e)
        {
            Intent intent = new Intent(context, typeof(AddTaskActivity));

            intent.SetFlags(ActivityFlags.ClearTask | ActivityFlags.ClearTop); //Empty activity stack
            intent.PutExtra("pos", (int)((View)sender).Tag); //Send position of the item in the list
            context.StartActivity(intent);
        }

        /// <summary>
        /// marks the clicked task as completed/not completed
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private async void Task_ClickAsync(object sender, EventArgs e)
        {
            Console.WriteLine("CLICKED! " + sender.GetType() + ", " + ((View)sender).Tag);

            LinearLayout row;
            if (((View)sender).GetType() == typeof(TextView) || ((View)sender).GetType() == typeof(ImageView))
            {
                row = (LinearLayout)((View)sender).Parent;
            }
            else
            {
                row = (LinearLayout)sender;
            }

            Task chosenTask = YourTasksActivity.tasksList[(int)((View)sender).Tag];
            chosenTask.IsCompleted = !chosenTask.IsCompleted;
            chosenTask.TickImg = BitmapFactory.DecodeResource(Application.Context.Resources, context.Resources.GetIdentifier(chosenTask.GetTickImgFilePath(), "drawable", context.PackageName));
            this.tasks[(int)((View)sender).Tag] = YourTasksActivity.tasksList[(int)((View)sender).Tag];

            string key = await firebaseHelper.GetTaskListKey(MainActivity.loginUser.listCode);
            firebaseHelper.UpdateExistingTask(chosenTask, key);

            //Change task status image
            ImageView tick = (ImageView)row.GetChildAt(0);
            tick.SetImageBitmap(chosenTask.TickImg);

            //Change title
            TextView taskTitle = (TextView)row.GetChildAt(1);
            taskTitle.PaintFlags = chosenTask.IsCompleted ? PaintFlags.StrikeThruText : PaintFlags.LinearText;

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.tasks_layout, parent, false);

            TaskViewHolder vh = new TaskViewHolder(itemView);
            return vh;
        }

        public override int ItemCount {
            get
            {
                return this.tasks.Count;
            }
        }
    }

    class TaskAdapterViewHolder : Java.Lang.Object
    {
        //Your adapter views to re-use
    }
}