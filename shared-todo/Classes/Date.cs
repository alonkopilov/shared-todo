﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo.Classes
{
    public class Date
    {
        public Date()
        {

        }

        public Date(string date)
        {
            if (date == "No Date")
            {
                this.Day = 0;
                this.Month = 0;
                this.Year = 0;
            }
            else
            {
                string[] dateParts = date.Split('.');
                this.Day = int.Parse(dateParts[0]);
                this.Month = int.Parse(dateParts[1]);
                this.Year = int.Parse(dateParts[2]);
            }           
        }

        public Date(int day, int month, int year)
        {
            Day = day;
            Month = month;
            Year = year;
        }

        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }

        public override string ToString()
        {
            if (Day != 0)
            {
                return Day + "." + Month + "." + Year;
            }
            return "No Date";
        }
    }
}