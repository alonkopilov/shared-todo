﻿using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using System;

namespace shared_todo.Classes
{
    [Service]
    class MusicService : Service
    {
        private MediaPlayer mp;
        static bool serviceDestroyed = false;
        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnCreate()
        {
            base.OnCreate();
            mp = MediaPlayer.Create(this, Resource.Raw.bg_music);
            mp.Looping = true;
            mp.Completion += MediaPlayerCompletion;
            mp.SetVolume(60, 60);
        }

        private void MediaPlayerCompletion(object sender, EventArgs e)
        {
            Toast.MakeText(this, "Music Ended, Replaying..", ToastLength.Long).Show();
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            CreateNotificationChannel();

            ISharedPreferences sp;
            sp = this.GetSharedPreferences("playbackDetails", FileCreationMode.Private);
            serviceDestroyed = false;

            if (sp != null)
            {
                int newPos = sp.GetInt("currPos", 0);
                mp.SeekTo(newPos);
            }
            mp.Start();

            Intent NotificationIntent = new Intent(this, typeof(MainActivity));
            PendingIntent pendingIntent = PendingIntent.GetActivity(this, requestCode: 0, NotificationIntent, flags: 0);
            
            Notification notification = new AndroidX.Core.App.NotificationCompat.Builder(context: this, channelId: "ChannelID1")
                    .SetContentTitle(Resources.GetString(Resource.String.app_name))
                    .SetContentText("Playing background music")
                    .SetSmallIcon(Resource.Drawable.sharedtodo_logo)
                    .SetContentIntent(pendingIntent)
                    .SetOngoing(true)
                    .Build();

            StartForeground(id: 1, notification);

            return StartCommandResult.Sticky;
        }

        private void CreateNotificationChannel()
        {
            NotificationChannel notificationChannel = new NotificationChannel(id: "ChannelID1", name: "Forground Notification",
                                                                            importance: NotificationImportance.Default);
            NotificationManager manager = (NotificationManager)GetSystemService(Context.NotificationService);
            manager.CreateNotificationChannel(notificationChannel);
        }

        public override void OnDestroy()
        {
            if (serviceDestroyed)
            {
                return;
            }

            ISharedPreferences sp = this.GetSharedPreferences("playbackDetails", FileCreationMode.Private);
            Android.Content.ISharedPreferencesEditor spEditor = sp.Edit();
            spEditor.PutInt("currPos", mp.CurrentPosition);
            spEditor.Commit();

            if (mp != null)
            {
                mp.Stop();
                mp.Release();
            }
            
            base.OnDestroy();
            StopForeground(true);
            StopSelf();
            serviceDestroyed = true;
        }
    }
}