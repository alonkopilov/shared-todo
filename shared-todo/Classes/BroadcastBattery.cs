﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo.Classes
{
    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { Intent.ActionBatteryChanged, Intent.ActionBatteryOkay })]
    public class BroadcastBattery : BroadcastReceiver
    {
        private TextView batteryText;
        private ImageView isCharging;
        public override void OnReceive(Context context, Intent intent)
        {
            const int BATTERY_STATUS_FULL = 5, BATTERY_STATUS_CHARGING = 2;
            int isPlugged = intent.GetIntExtra(BatteryManager.ExtraStatus, 0);
            int battery = intent.GetIntExtra("level", 0);

            batteryText.Text = battery + "%";

            //Check charging
            isCharging.Visibility = (isPlugged == BATTERY_STATUS_CHARGING || isPlugged == BATTERY_STATUS_FULL) ? ViewStates.Visible : ViewStates.Invisible;
        }

        public BroadcastBattery() : base()
        {
        }

        public BroadcastBattery(TextView btryText, ImageView chargingIcon) : base()
        {
            batteryText = btryText;
            isCharging = chargingIcon;
        }
    }
}