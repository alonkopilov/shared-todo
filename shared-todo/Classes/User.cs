﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo.Classes
{
    public class User
    {
        public User()
        {
        }

        public User(string username, string password, string email, string listCode)
        {
            this.username = username;
            this.password = password;
            this.email = email;
            this.listCode = listCode;
            this.profilePic = null;
        }

        public User(string userId, string username, string password, string email, string listCode, Bitmap profilePic)
        {
            this.userId = userId;
            this.username = username;
            this.password = password;
            this.email = email;
            this.listCode = listCode;
            this.profilePic = profilePic;
        }

        public string userId { get; set; }

        public string username { get; set; }

        public string password { get; set; }

        public string email { get; set; }

        public string listCode { get; set; }

        public Bitmap profilePic { get; set; }

        public override string ToString()
        {
            return userId + ", " + username + ", " + password + ", " + email + ", " + listCode; 
        }

    }
}