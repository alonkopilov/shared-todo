﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo.Classes
{
    public class TaskList
    {
        public TaskList()
        {

        }

        public TaskList(string listCode, List<Task> tasks)
        {
            this.ListCode = listCode;
            this.Tasks = tasks;
        }

        public TaskList(string tasksListId, string listCode, List<Task> tasks)
        {
            TasksListId = tasksListId;
            ListCode = listCode;
            Tasks = tasks;
        }

        public string TasksListId { get; set; }
        public string ListCode { get; set; }
        public List<Task> Tasks { get; set; }
    }
}