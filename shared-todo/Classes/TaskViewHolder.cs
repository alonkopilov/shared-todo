﻿using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;

namespace shared_todo.Classes
{
    public class TaskViewHolder : RecyclerView.ViewHolder
    {
        public TextView tvTaskName;
        public TextView tvDueDate;
        public ImageView ivTickImg;
        public LinearLayout layout;

        public TaskViewHolder(View itemView) : base(itemView)
        {
            tvTaskName = itemView.FindViewById<TextView>(Resource.Id.tvName);
            tvDueDate = itemView.FindViewById<TextView>(Resource.Id.tvDate);
            ivTickImg = itemView.FindViewById<ImageView>(Resource.Id.ivTick);
            layout = itemView.FindViewById<LinearLayout>(Resource.Id.llTaskLayout);
        }
    }
}