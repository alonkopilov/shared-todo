﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo.Classes
{
    public class Task
    {
        public Task() { }
        public Task(string taskId, string name, string category, string description, string color, Bitmap tickImg, Date dueDate, bool isCompleted)
        {
            TaskId = taskId;
            Name = name;
            Category = category;
            Description = description;
            Color = color;
            TickImg = tickImg;
            DueDate = dueDate ?? new Date(0, 0, 0);
            IsCompleted = isCompleted;
        }
 
        public Task(Task other)
        {
            this.Name = other.Name;
            this.Category = other.Category;
            this.Description = other.Description;
            this.Color = other.Color;
            this.TickImg = other.TickImg;
            this.DueDate = other.DueDate;
            this.TaskId = other.TaskId;
        }

        /// <summary>
        /// Returns the path to resources of the tick image, 
        /// based of if the task is completed, and the color of the task category
        /// </summary>
        /// <returns></returns>
        public string GetTickImgFilePath()
        {
            return this.Color + (IsCompleted ? "_tick_filled" : "_tick");
        }

        public string Name { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
        public Bitmap TickImg { get; set; }
        public Date DueDate { get; set; }
        public string TaskId { get; set; }

        public bool IsCompleted { get; set; }
    }
}