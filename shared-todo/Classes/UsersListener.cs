﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Firebase;
using Firebase.Database;
using Android.Graphics;

namespace shared_todo.Classes
{
    class UsersListener : Java.Lang.Object, IValueEventListener
    {
        private List<User> users;
        private FirebaseHelper firebaseHelper = new FirebaseHelper(null);

        public class UsersDataEventArgs : EventArgs
        {
            public List<User> allUsers { get; set; }
        }
        public event EventHandler<UsersDataEventArgs> UsersRetrived;

        public void Create()
        {
            DatabaseReference databaseReference = firebaseHelper.GetDatabase().GetReference("Users");

            databaseReference.AddValueEventListener(this);
        }

        /// <summary>
        /// Updates the list of users from the database
        /// </summary>
        /// <param name="snapshot">A database snapshot to fetch the data from</param>
        public void OnDataChange(DataSnapshot snapshot)
        {
            if (snapshot != null)
            {
                var child = snapshot.Children.ToEnumerable<DataSnapshot>();
                users = new List<User>();

                foreach (DataSnapshot userData in child)
                {
                    Bitmap pic = null;
                    Java.Lang.Object d = userData.Child("PROFILE_IMG").Value;
                    if (d != null)
                    {
                        pic = firebaseHelper.StringToBitMap(d.ToString());
                    }

                    User user = new User(userData.Key,
                        userData.Child("USERNAME").Value.ToString(),
                        userData.Child("PASSWORD").Value.ToString(),
                        userData.Child("EMAIL").Value.ToString(),
                        userData.Child("LISTCODE").Value.ToString(),
                        pic);

                    this.users.Add(user);
                }
                UsersRetrived.Invoke(this, new UsersDataEventArgs { allUsers = users });
            }
        }
        public void OnCancelled(DatabaseError error)
        {
        } 

    }
}