﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Firebase;
using Firebase.Database;
using Android.Graphics;

namespace shared_todo.Classes
{
    class TaskListsListener : Java.Lang.Object, IValueEventListener
    {
        private List<TaskList> taskLists;
        private Context appContext;
        private FirebaseHelper firebaseHelper;

        public TaskListsListener(Context context)
        {
            this.appContext = context;
            this.firebaseHelper = new FirebaseHelper(context);
        }

        public class ListsDataEventArgs : EventArgs
        {
            public List<TaskList> allTasksLists { get; set; }
        }
        public event EventHandler<ListsDataEventArgs> ListsRetrived;

        public void Create()
        {
            DatabaseReference databaseReference = firebaseHelper.GetDatabase().GetReference("Lists");

            databaseReference.AddValueEventListener(this);
        }

        /// <summary>
        /// Updates the list of task lists from the database
        /// </summary>
        /// <param name="snapshot">A database snapshot to fetch the data from</param>
        public void OnDataChange(DataSnapshot snapshot)
        {
            if (snapshot != null)
            {
                var child = snapshot.Children.ToEnumerable<DataSnapshot>();
                taskLists = new List<TaskList>();

                foreach (DataSnapshot listData in child)
                {
                    string listCode = listData.Child("LISTCODE").Value.ToString();
                    List<Task> tasksList = GetListTasks(listData.Child("TASKS"));

                    TaskList taskList = new TaskList(listData.Key, listCode, tasksList);

                    taskLists.Add(taskList);
            
                }
                ListsRetrived.Invoke(this, new ListsDataEventArgs { allTasksLists = taskLists });
            }
        }

        /// <summary>
        /// Fetches the list of tasks from a given child of tasks list in the database
        /// </summary>
        /// <param name="listsSnapshot">A database snapshot of the task list child to fetch the data from</param>
        /// <returns>The list of tasks in that child</returns>
        public List<Task> GetListTasks(DataSnapshot listsSnapshot)
        {
            if (listsSnapshot != null)
            {
                List<Task> tasks = new List<Task>();
                var child = listsSnapshot.Children.ToEnumerable<DataSnapshot>();

                foreach (DataSnapshot listData in child)
                {
                    string name = listData.Child("NAME").Value.ToString();
                    string category = listData.Child("CATEGORY").Value.ToString();
                    string description = listData.Child("DESCRIPTION").Value.ToString();
                    string color = listData.Child("COLOR").Value.ToString();
                    Bitmap tickImg = BitmapFactory.DecodeResource(Application.Context.Resources, appContext.Resources.GetIdentifier(color + "_tick", "drawable", appContext.PackageName));
                    Date dueDate = new Date(listData.Child("DUE_DATE").Value.ToString());
                    bool isCompleted = bool.Parse(listData.Child("IS_COMPLETED").Value.ToString());

                    tasks.Add(new Task(listData.Key, name, category, description, color, tickImg, dueDate, isCompleted));
                }

                return tasks;
            }
            return null;
        }

        public void OnCancelled(DatabaseError error)
        {
        }
    }
}