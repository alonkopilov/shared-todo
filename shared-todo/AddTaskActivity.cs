﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Icu.Util;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using shared_todo.Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Android.Icu.Util;
using Android.Content.PM;

namespace shared_todo
{
    [Activity(Label = "AddTaskActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AddTaskActivity : Activity, DatePickerDialog.IOnDateSetListener
    {
        private Button selectTask, confirmTask, cancel, selectDate;
        private EditText description;
        private TextView selectedDate, title;
        private FirebaseHelper firebaseHelper;
        private Task task;
        private int pos;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.edit_layout);
            InitStatusBar();

            selectTask = FindViewById<Button>(Resource.Id.btnSelectTask);
            selectDate = FindViewById<Button>(Resource.Id.btnSelectDate);
            confirmTask = FindViewById<Button>(Resource.Id.btnAddTask);
            cancel = FindViewById<Button>(Resource.Id.btnCancel);
            description = FindViewById<EditText>(Resource.Id.etDescription);
            selectedDate = FindViewById<TextView>(Resource.Id.tvSelectedDate);
            title = FindViewById<TextView>(Resource.Id.tvTitle);
            firebaseHelper = new FirebaseHelper(this);

            task = new Task();
            pos = Intent.GetIntExtra("pos", -1);

            if (pos != -1) //Existing task to edit
            {
                Task temp = YourTasksActivity.tasksList[pos];

                this.task = new Task(temp);

                selectTask.Text = temp.Name;
                selectedDate.Text = (temp.DueDate != null) ? temp.DueDate.ToString() : "";
                
                description.Text = temp.Description;
            }
            else
            {
                Toast.MakeText(this, "Adding new item..", ToastLength.Short).Show();
            }

            cancel.Click += GoToYourTasks;
            selectTask.Click += GoToTaskChoices;
            confirmTask.Click += SaveTask;
            selectDate.Click += delegate
            {
                OnClickDateEditText();
            };

            InitFonts();
        }

        /// <summary>
        /// Initializes fonts required for all text on screen 
        /// </summary>
        private void InitFonts()
        {
            Typeface titleTypeface = Typeface.CreateFromAsset(this.Assets, "Nexa Bold.otf");
            Typeface textTypeface = Typeface.CreateFromAsset(this.Assets, "Montserrat-Regular.ttf");

            title.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            selectTask.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            confirmTask.SetTypeface(textTypeface, TypefaceStyle.Normal);
            cancel.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            selectDate.SetTypeface(titleTypeface, TypefaceStyle.Normal);
        }

        /// <summary>
        /// Directs to "Your Tasks" screen, discards unfinished edits.
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoToYourTasks(object sender, EventArgs e)
        {
            Intent iYourTasks = new Intent(this, typeof(YourTasksActivity));
            iYourTasks.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);

            StartActivity(iYourTasks);
        }

        /// <summary>
        /// Saves the existing/new task.
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private async void SaveTask(object sender, EventArgs e)
        {
            if (selectTask.Text != "SELECT TASK")
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                this.task.Description = description.Text;
                if (pos != -1)  //Update Existing item
                {
                    YourTasksActivity.tasksList[pos] = new Task(this.task);
                    string key = await firebaseHelper.GetTaskListKey(MainActivity.loginUser.listCode);
                    firebaseHelper.UpdateExistingTask(task, key);
                }
                else   //Create new task
                {
                    YourTasksActivity.tasksList.Add(new Task(this.task)); // Add to the end of the list 

                    DatabaseReference reference = firebaseHelper.GetDatabase().GetReference("Lists").Push();
                    string listKey = await firebaseHelper.GetTaskListKey(MainActivity.loginUser.listCode);

                    firebaseHelper.AddTaskToList(reference, task, listKey);           
                }
                stopwatch.Stop();
                Console.WriteLine($"[SaveTask] -> {stopwatch.ElapsedMilliseconds}");
                Finish();
            }
            else
            {
                Toast.MakeText(this, "Select a task first", ToastLength.Short).Show();
            }
        }

        /// <summary>
        /// Shows the DatePickerDialog for the user
        /// </summary>
        private void OnClickDateEditText()
        {
            var dateTimeNow = DateTime.Now;
            Calendar calendar = Calendar.GetInstance(Android.Icu.Util.TimeZone.Default);
            DatePickerDialog datePicker = new DatePickerDialog(this, this, dateTimeNow.Year, dateTimeNow.Month, dateTimeNow.Day);

            //Sets the minimum date the user can choose as the current day
            datePicker.DatePicker.MinDate = calendar.TimeInMillis;
            datePicker.UpdateDate(calendar.Get(CalendarField.Year), calendar.Get(CalendarField.Month) - 1, calendar.Get(CalendarField.Date));
            datePicker.Show();
        }

        /// <summary>
        /// Sets the chosen date in the DatePickerDialog as the deadline date for the task. 
        /// </summary>
        /// <param name="view">DatePicker object that the date was set on</param>
        /// <param name="year">int chosen year</param>
        /// <param name="month">int chosen month</param>
        /// <param name="dayOfMonth">int chosen day in month</param>
        public void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            Date date = new Date(dayOfMonth, month + 1, year);

            selectedDate.Text = date.ToString();
            task.DueDate = date;
        }

        /// <summary>
        /// Directs to the "Task Choices" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoToTaskChoices(object sender, EventArgs e)
        {
            Intent iTaskChoices = new Intent(this, typeof(TaskChoicesActivity));
            
            StartActivityForResult(iTaskChoices, 100);
        }

        /// <summary>
        /// Saves the task choice chosen in the "Task Choices" screen 
        /// </summary>
        /// <param name="requestCode">int request code</param>
        /// <param name="resultCode">Activity result code</param>
        /// <param name="data">Intent data from called activity</param>
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == 100)
            {
                if (resultCode == Result.Ok)
                {
                    string[] result = data.Extras.GetStringArray("result");

                    task.Category = result[0];
                    task.Color = result[1];
                    task.Name = result[2];
                    task.TickImg = BitmapFactory.DecodeResource(Application.Context.Resources, this.Resources.GetIdentifier(task.Color + "_tick", "drawable", this.PackageName));

                    selectTask.Text = task.Name;
                }
            }
        }

        /// <summary>
        /// Initialize transparent status bar
        /// </summary>
        public void InitStatusBar()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
                var s = SystemUiFlags.LayoutFullscreen | SystemUiFlags.LayoutStable;
                FindViewById(Android.Resource.Id.Content).SystemUiVisibility = (StatusBarVisibility)s;
            }
        }
    }
}