﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using shared_todo.Classes;
using Button = Android.Widget.Button;
using CheckBox = Android.Widget.CheckBox;
using ProgressBar = Android.Widget.ProgressBar;

namespace shared_todo
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity, MediaPlayer.IOnPreparedListener
    {
        private FirebaseHelper firebaseHelper;
        private ProgressBar progressBar;
        private EditText username, password;
        private Button login;
        private TextView register, title, regMsg, rememberText;
        private CheckBox rememberMe;
        private VideoView videoView;
        private Intent musicPlay;
        private ImageView muteUnmute;
        private static bool isPlaying;
        private ISharedPreferences sp;
        public static User loginUser;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState); // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            InitStatusBar();

            username = FindViewById<EditText>(Resource.Id.etUsername);
            password = FindViewById<EditText>(Resource.Id.etPassword);
            login = FindViewById<Button>(Resource.Id.btnEnterApp);
            register = FindViewById<TextView>(Resource.Id.tvRegisterTextBtn);
            rememberMe = FindViewById<CheckBox>(Resource.Id.cbRememberMe);
            progressBar = FindViewById<ProgressBar>(Resource.Id.pBar);
            videoView = FindViewById<VideoView>(Resource.Id.vvBG);
            title = FindViewById<TextView>(Resource.Id.tvTitle);
            regMsg = FindViewById<TextView>(Resource.Id.tvRegQuestion);
            rememberText = FindViewById<TextView>(Resource.Id.tvRememberText);
            muteUnmute = FindViewById<ImageView>(Resource.Id.ivMute);
            sp = this.GetSharedPreferences("details", FileCreationMode.Private);
            musicPlay = new Intent(this, typeof(MusicService));
            firebaseHelper = new FirebaseHelper(this);

            muteUnmute.Click += PlayOrPause;
            login.Click += LoginToApp;
            register.Click += GoToSignup;

            progressBar.Visibility = ViewStates.Invisible;

            InitBackground();
            InitMusicLogo();
            InitFonts();

            checkUserRemembered();
        }
        
        /// <summary>
        /// Initializes the "turn music on/off" logo
        /// </summary>
        public void InitMusicLogo()
        {
            if (!isPlaying)
            {
                muteUnmute.SetBackgroundResource(Resource.Drawable.logo_mute);
            }
            else
            {
                muteUnmute.SetBackgroundResource(Resource.Drawable.logo_not_muted);
            }
        }

        /// <summary>
        /// Initializes the video background
        /// </summary>
        private void InitBackground()
        {
            videoView.SetVideoPath("android.resource://" + this.PackageName + "/" + Resource.Drawable.video);
            videoView.SetOnPreparedListener(this);
            videoView.Start();
        }

        /// <summary>
        /// Plays or pauses the music according to it's current state.
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void PlayOrPause(object sender, System.EventArgs e)
        {
            if (sender == muteUnmute)
            {
                if (!isPlaying)
                {
                    StartService(musicPlay);
                    muteUnmute.SetBackgroundResource(Resource.Drawable.logo_not_muted);
                }
                else
                {
                    StopService(musicPlay);
                    muteUnmute.SetBackgroundResource(Resource.Drawable.logo_mute);
                }
                isPlaying = !isPlaying;
            }
        }

        /// <summary>
        /// Initializes fonts required for all text on screen 
        /// </summary>
        private void InitFonts()
        {
            Typeface titleTypeface = Typeface.CreateFromAsset(this.Assets, "Nexa Bold.otf");
            Typeface textTypeface = Typeface.CreateFromAsset(this.Assets, "Montserrat-Regular.ttf");
            register.SetTypeface(textTypeface, TypefaceStyle.Normal);
            title.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            regMsg.SetTypeface(textTypeface, TypefaceStyle.Normal);
            register.SetTypeface(textTypeface, TypefaceStyle.Normal);
            username.SetTypeface(textTypeface, TypefaceStyle.Normal);
            password.SetTypeface(textTypeface, TypefaceStyle.Normal);
            login.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            rememberText.SetTypeface(textTypeface, TypefaceStyle.Normal);
        }

        /// <summary>
        /// Directs to the "Signup" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoToSignup(object sender, System.EventArgs e)
        {
            Intent iSignup = new Intent(this, typeof(SignupActivity));

            StartActivity(iSignup);
        }

        /// <summary>
        /// Logins the user
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private async void LoginToApp(object sender, System.EventArgs e)
        {
            progressBar.Visibility = ViewStates.Visible;
            User user = await this.firebaseHelper.GetUser(this.username.Text);

            if (username.Text == string.Empty || password.Text == string.Empty) //All fields were not filled 
            {
                Toast.MakeText(this, "Please fill all fields.", ToastLength.Short).Show();
            }
            else if (user != null && password.Text == user.password) //Login was successful
            {
                Intent iYourTasks = new Intent(this, typeof(YourTasksActivity));
                iYourTasks.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);
                loginUser = user;

                RememberUser(username.Text);
                StartActivity(iYourTasks);
            }
            else //Username or password are not correct
            {
                Toast.MakeText(this, "Username or password don't match.", ToastLength.Short).Show();
            }
            progressBar.Visibility = ViewStates.Invisible;
        }

        /// <summary>
        /// Remembers the user in a SharedPreferences for automatic logins.
        /// </summary>
        /// <param name="username">string username to remember</param>
        public void RememberUser(string username)
        {
            if (rememberMe.Checked)
            {
                ISharedPreferencesEditor editor = sp.Edit();

                editor.PutBoolean("isRemembered", true);
                editor.PutString("currentLoginedUser", username);
                editor.Commit();
            }
        }

        /// <summary>
        /// Checks if the current user is remembered. If he is he gets automatically connected
        /// </summary>
        public async void checkUserRemembered()
        {
            if (sp.GetBoolean("isRemembered", false))
            {
                progressBar.Visibility = ViewStates.Visible;

                User user = await this.firebaseHelper.GetUser(sp.GetString("currentLoginedUser", ""));
                Intent iYourTasks = new Intent(this, typeof(YourTasksActivity));

                iYourTasks.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);
                loginUser = user;

                progressBar.Visibility = ViewStates.Invisible;

                StartActivity(iYourTasks);
            }
        }

        /// <summary>
        /// Initializes permissions after request.
        /// </summary>
        /// <param name="requestCode">int request code</param>
        /// <param name="permissions">string array of permissions requested</param>
        /// <param name="grantResults">Permissions array of all denied and granted permissions</param>
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        /// <summary>
        /// Loops the video background when activity loaded
        /// </summary>
        /// <param name="mp">MediaPlayer to play</param>
        public void OnPrepared(MediaPlayer mp)
        {
            videoView.Start();
            mp.Looping = true;
        }

        /// <summary>
        /// Initialize transparent status bar
        /// </summary>
        public void InitStatusBar()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
                var s = SystemUiFlags.LayoutFullscreen | SystemUiFlags.LayoutStable;
                FindViewById(Android.Resource.Id.Content).SystemUiVisibility = (StatusBarVisibility)s;
            }
        }
    }
}