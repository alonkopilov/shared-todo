﻿using AbdulAris.Civ;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.Core.App;
using AndroidX.Core.Content;
using shared_todo.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shared_todo
{
    [Activity(Label = "YourAccountActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class YourAccountActivity : Activity, ActivityCompat.IOnRequestPermissionsResultCallback
    {
        private CircleImageView profileImg, addPicture;
        private TextView name, password, email, listCode;
        private ImageView goBack;
        private FirebaseHelper firebaseHelper;
        private LinearLayout participantsListLayout;
        private const int REQUEST_CAMERA = 0;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_account);
            InitStatusBar();
            
            name = FindViewById<TextView>(Resource.Id.tvTitle);
            password = FindViewById<TextView>(Resource.Id.tvPassword);
            email = FindViewById<TextView>(Resource.Id.tvEmail);
            listCode = FindViewById<TextView>(Resource.Id.tvListCode);
            profileImg = FindViewById<CircleImageView>(Resource.Id.cimProfile);
            addPicture = FindViewById<CircleImageView>(Resource.Id.cimChangePic);
            goBack = FindViewById<ImageView>(Resource.Id.ibGoBack);
            participantsListLayout = FindViewById<LinearLayout>(Resource.Id.llPartList);
            firebaseHelper = new FirebaseHelper(this);

            name.Text = MainActivity.loginUser.username;
            password.Text = MainActivity.loginUser.password;
            email.Text = MainActivity.loginUser.email;
            listCode.Text = MainActivity.loginUser.listCode;

            addPicture.Click += AddPicture;
            goBack.Click += GoBackToPreviousScreen;

            UpdateExistingPic();
            InitFonts();
            AddParticipantsList();
        }

        /// <summary>
        /// Creates the participants list from the list the user is currently on, and shows it on the screen
        /// </summary>
        private async void AddParticipantsList()
        {
            List<User> usersInTaskList = await firebaseHelper.GetUsersInList(MainActivity.loginUser.listCode);

            foreach (User user in usersInTaskList)
            {
                // Create layout for user data
                LinearLayout participantLayout = new LinearLayout(this);
                LinearLayout.LayoutParams parameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
                parameters.SetMargins(0, 20, 0, 0);
                participantLayout.LayoutParameters = parameters;
                participantLayout.Orientation = Orientation.Horizontal;

                // Add the user's name and profile picture to the layout
                participantLayout.AddView(CreateParticipantProfilePic(user.profilePic));
                participantLayout.AddView(CreateParticipantTitle(user.username));
                participantsListLayout.AddView(participantLayout);
            }
        }

        /// <summary>
        /// Creates the user's ImageView profile pic
        /// </summary>
        /// <param name="pic">The user's bitmap profile picture</param>
        /// <returns>CircleImageView pic of the user</returns>
        private CircleImageView CreateParticipantProfilePic(Bitmap pic)
        {
            CircleImageView participantPic = new CircleImageView(this);
            participantPic.LayoutParameters = new ViewGroup.LayoutParams(150, 150);
            participantPic.SetImageBitmap(pic);

            return participantPic;
        }


        /// <summary>
        /// Creates the user's Textview title
        /// </summary>
        /// <param name="username">The user's name</param>
        /// <returns>TextView title of the user</returns>
        private TextView CreateParticipantTitle(string username)
        {
            TextView participantName = new TextView(this);
            Typeface textTypeface = Typeface.CreateFromAsset(this.Assets, "Montserrat-Regular.ttf");
            LinearLayout.LayoutParams parameters2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.MatchParent);
            parameters2.SetMargins(40, 0, 0, 0);
            participantName.LayoutParameters = parameters2;
            participantName.Gravity = GravityFlags.CenterVertical;
            participantName.Text = username;
            participantName.SetTextColor(Color.Black);
            participantName.TextSize = 20;
            participantName.SetTypeface(textTypeface, TypefaceStyle.Normal);

            return participantName;
        }

        /// <summary>
        /// Directs to the "settings" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoBackToPreviousScreen(object sender, EventArgs e)
        {
            Finish();
        }

        /// <summary>
        /// Updates the current profile pic of the logined user
        /// </summary>
        private async void UpdateExistingPic()
        {
            if (MainActivity.loginUser.userId == null)
            {
                MainActivity.loginUser.userId = (await firebaseHelper.GetUser(MainActivity.loginUser.username)).userId;
            } 
            Bitmap pic = await this.firebaseHelper.GetUserPic(MainActivity.loginUser);

            profileImg.SetImageBitmap(pic);
        }

        /// <summary>
        /// Sets a new profile picture for the user. Directs the user to the camera
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void AddPicture(object sender, EventArgs e)
        {
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.Camera) == (int)Permission.Granted)
            {
                Intent intent = new Intent(Android.Provider.MediaStore.ActionImageCapture);
                StartActivityForResult(intent, 0);
            }
            else if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.Camera))
            {
                Android.App.AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.SetTitle("Camera permission is needed");
                builder.SetMessage("Camera access is required for changing your profile image");
                builder.SetCancelable(true);
                builder.SetPositiveButton("Allow", (sender, id) =>
                {
                    ActivityCompat.RequestPermissions(this, new string[] { Android.Manifest.Permission.Camera }, 0);
                });
                builder.SetNegativeButton("Deny", (sender, id) => { });
                builder.Create().Show();
            }
            else
            {
                ActivityCompat.RequestPermissions(this, new string[] { Android.Manifest.Permission.Camera }, 0);
            }
        }

        /// <summary>
        /// Initializes fonts required for all text on screen 
        /// </summary>
        private void InitFonts()
        {
            Typeface titleTypeface = Typeface.CreateFromAsset(this.Assets, "Nexa Bold.otf");
            Typeface textTypeface = Typeface.CreateFromAsset(this.Assets, "Montserrat-Regular.ttf");
            
            name.SetTypeface(titleTypeface, TypefaceStyle.Normal);
            password.SetTypeface(textTypeface, TypefaceStyle.Normal);
            email.SetTypeface(textTypeface, TypefaceStyle.Normal);
            listCode.SetTypeface(textTypeface, TypefaceStyle.Normal);
        }

        /// <summary>
        /// Saves the task choice chosen in the "Task Choices" screen 
        /// </summary>
        /// <param name="requestCode">int request code</param>
        /// <param name="resultCode">Activity result code</param>
        /// <param name="data">Intent data from called activity</param>
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CAMERA) //Coming from camera
            {
                if (resultCode == Result.Ok)
                {
                    Bitmap newProfileImg = (Android.Graphics.Bitmap)data.Extras.Get("data");

                    profileImg.SetImageBitmap(newProfileImg);
                    firebaseHelper.UpdatePicToUser(MainActivity.loginUser, newProfileImg);
                }
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode == REQUEST_CAMERA)
            {
                if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                {
                    Toast.MakeText(this, "Permission Granted", ToastLength.Short).Show();
                    Intent intent = new Intent(Android.Provider.MediaStore.ActionImageCapture);
                    StartActivityForResult(intent, REQUEST_CAMERA);
                }
                else
                {
                    Toast.MakeText(this, "Camera access is required for changing your profile image", ToastLength.Short).Show();
                }
            }
            else
            {
                base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            }      
        }

        /// <summary>
        /// Initialize transparent status bar
        /// </summary>
        public void InitStatusBar()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
                var s = SystemUiFlags.LayoutFullscreen | SystemUiFlags.LayoutStable;
                FindViewById(Android.Resource.Id.Content).SystemUiVisibility = (StatusBarVisibility)s;
            }
        }
    }
}