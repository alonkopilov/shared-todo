﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using shared_todo.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo
{
    [Activity(Label = "TaskChoicesActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class TaskChoicesActivity : Activity
    {
        private LinearLayout tasksLayout;
        private ImageView goBack;
        private TextView title;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_taskchoices);
            InitStatusBar();

            tasksLayout = FindViewById<LinearLayout>(Resource.Id.llTasks);
            goBack = FindViewById<ImageView>(Resource.Id.ibGoBack);
            title = FindViewById<TextView>(Resource.Id.tvTitle);

            goBack.Click += GoBackToPreviousScreen;
            SetButtons(GetAllLayoutButtons(tasksLayout));

            InitFonts();
        }

        /// <summary>
        /// Initializes fonts required for all text on screen 
        /// </summary>
        private void InitFonts()
        {
            Typeface textTypeface = Typeface.CreateFromAsset(this.Assets, "Nexa Bold.otf");

            title.SetTypeface(textTypeface, TypefaceStyle.Normal);
        }

        /// <summary>
        /// Directs to the "Edit Task" screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoBackToPreviousScreen(object sender, EventArgs e)
        {
            Intent iEditTask = new Intent(this, typeof(AddTaskActivity));
            SetResult(Result.Canceled, iEditTask);
            Finish();
        }

        /// <summary>
        /// Initializes the list of task choices buttons
        /// </summary>
        /// <param name="layout">Layout that contains all the choices buttons</param>
        /// <returns>List of task choices buttons</returns>
        public List<Button> GetAllLayoutButtons(LinearLayout layout)
        {
            List<Button> btns = new List<Button>();
            Typeface textTypeface = Typeface.CreateFromAsset(this.Assets, "Montserrat-Regular.ttf");

            for (int i = 0; i < layout.ChildCount; i++)
            {
                View v = layout.GetChildAt(i);

                if (v is Button btn)
                {
                    btn.SetTypeface(textTypeface, TypefaceStyle.Normal);
                    btns.Add(btn);
                }
                if (v is TextView tv)
                {
                    tv.SetTypeface(textTypeface, TypefaceStyle.Normal);
                }
            }

            return btns;
        }

        /// <summary>
        /// Sets all task choices buttons as clickable
        /// </summary>
        /// <param name="btns">List of task choices buttons</param>
        public void SetButtons(List<Button> btns)
        {
            foreach (Button btn in btns)
            {
                btn.Click += ChooseTask;
            }
        }

        /// <summary>
        /// Chooses a clicked task, returns the task's data to the calling activity 
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void ChooseTask(object sender, EventArgs e)
        {
            Intent iEditTask = new Intent(this, typeof(AddTaskActivity));
          
            Button chosenTask = (Button)sender;

            string[] arr = new string[3];
            arr[0] = chosenTask.ContentDescription.Split(',')[0];   //Category
            arr[1] = chosenTask.ContentDescription.Split(',')[1];   //Color
            arr[2] = chosenTask.Text;                               //Name

            iEditTask.PutExtra("result", arr);
            SetResult(Result.Ok, iEditTask);
            Finish();
        }

        /// <summary>
        /// Initialize transparent status bar
        /// </summary>
        public void InitStatusBar()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
                var s = SystemUiFlags.LayoutFullscreen | SystemUiFlags.LayoutStable;
                FindViewById(Android.Resource.Id.Content).SystemUiVisibility = (StatusBarVisibility)s;
            }
        }
    }
}
