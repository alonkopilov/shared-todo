﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using shared_todo.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shared_todo
{
    [Activity(Label = "JoinListActivity", ScreenOrientation = ScreenOrientation.Portrait)]
    public class JoinListActivity : Activity
    {
        private EditText[] listCodeDigits;
        private Button confirmJoin;
        private ImageView goBack;
        private FirebaseHelper firebaseHelper;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_joinlist);
            InitStatusBar();

            firebaseHelper = new FirebaseHelper(this);

            InitDigits();
            confirmJoin = FindViewById<Button>(Resource.Id.btnConfirm);
            goBack = FindViewById<ImageView>(Resource.Id.ibGoBack);

            confirmJoin.Click += JoinNewList;
            goBack.Click += GoBackToPreviousScreen;
        }

        /// <summary>
        /// Puts focus on the next digit if a digit was entered, or on the previous digit if a digit was deleted
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void JoinListActivity_TextChanged(object sender, TextChangedEventArgs e)
        {
            int nextDigitIndex = Array.IndexOf(this.listCodeDigits, sender) + 1;
            int prevDigitIndex = nextDigitIndex - 2;
            int currentDigitIndex = nextDigitIndex - 1;

            if (currentDigitIndex > 0 && this.listCodeDigits[currentDigitIndex].Text == "") // If digit removed
            {
                this.listCodeDigits[prevDigitIndex].RequestFocus();
            }
            if (currentDigitIndex < 5 && this.listCodeDigits[currentDigitIndex].Text != "") // If digit added
            {
                this.listCodeDigits[nextDigitIndex].RequestFocus();
            }
        }

        /// <summary>
        /// Directs to settings screen
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private void GoBackToPreviousScreen(object sender, EventArgs e)
        {
            Finish();
        }

        /// <summary>
        /// Joins the list with the given list code entered, if it exists.
        /// </summary>
        /// <param name="sender">Event's data</param>
        /// <param name="e">Entity that raised the event</param>
        private async void JoinNewList(object sender, EventArgs e)
        {
            if (CheckAllDigitsFilled())
            {
                string newListCode = GetCompleteCodeFromArr();
                bool result = await this.firebaseHelper.JoinListForUser(MainActivity.loginUser, newListCode);
                
                if (result) 
                {
                    Intent iYourTasks = new Intent(this, typeof(YourTasksActivity));

                    Toast.MakeText(this, "List joined successfully!", ToastLength.Short).Show();
                    MainActivity.loginUser.listCode = newListCode;
                    StartActivity(iYourTasks);
                }
                else
                {
                    Toast.MakeText(this, "List does not exist", ToastLength.Short).Show();
                }
            }
            else
            {
                Toast.MakeText(this, "Please enter all digits", ToastLength.Short).Show();
            }
        }

        /// <summary>
        /// Checks if all the digits of the list code were filled
        /// </summary>
        /// <returns>True if all the digits of the list code were filled, else false</returns>
        private bool CheckAllDigitsFilled()
        {
            for (int i = 0; i < 6; i++)
            {
                if (this.listCodeDigits[i].Text == "")
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Builds the complete code string from digits entered
        /// </summary>
        /// <returns>string list code to join</returns>
        private string GetCompleteCodeFromArr()
        {
            string listCode = "";

            for (int i = 0; i < 6; i++)
            {
                listCode += this.listCodeDigits[i].Text;
            }

            return listCode;
        }

        /// <summary>
        /// Initializes EditTexts of list code digits
        /// </summary>
        private void InitDigits()
        {
            listCodeDigits = new EditText[6];

            for (int i = 0; i < 6; i++)
            {
                int resID = base.Resources.GetIdentifier("etD" + (i + 1), "id", PackageName);
                listCodeDigits[i] = FindViewById<EditText>(resID);
                listCodeDigits[i].TextChanged += JoinListActivity_TextChanged;
            }
        }

        /// <summary>
        /// Initialize transparent status bar
        /// </summary>
        public void InitStatusBar()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
                var s = SystemUiFlags.LayoutFullscreen | SystemUiFlags.LayoutStable;
                FindViewById(Android.Resource.Id.Content).SystemUiVisibility = (StatusBarVisibility)s;
            }
        }
    }
}